require('dotenv').config();

const { Telegraf, Markup } = require('telegraf');

if (process.env == undefi)

const bot = new Telegraf(process.env.botToken);

let state = '';
let calculationStep = 1;
let a = 0;
let b = 0;

const isDigit = (value) => {
	if (value.replace(/\s/g, '').length === 0)
		return false;
	else
		return true;
}

const nullState = () => {
	state = '';
}

const keyboard = Markup.inlineKeyboard([
  Markup.button.callback('+', 'sum'),
  Markup.button.callback('-', 'diff'),
  Markup.button.callback('*', 'multi'),
  Markup.button.callback('/', 'div')
]);

bot.command('calc', (ctx) => {
	ctx.reply('What do you want?', keyboard);
});

bot.on('message', (ctx) => {
	switch (state) {
		case 'sum':
			if (calculationStep === 1) {
				if (isDigit(ctx.message.text)) {
					a = Number(ctx.message.text);
					calculationStep++;
					ctx.reply('Ok. Remember it.');
				} else {
					ctx.reply('I wait a number!');
				}
			} else if (calculationStep === 2) {
					if (isDigit(ctx.message.text)) {
						b = Number(ctx.message.text);
						calculationStep = 1;
						ctx.reply(`Sum of ${a} and ${b}: ${a + b}`);
						nullState();
					} else {
						ctx.reply('I wait a number!');
					}
			}
			break;
		case 'diff':
			if (calculationStep === 1) {
				if (isDigit(ctx.message.text)) {
					a = Number(ctx.message.text);
					calculationStep++;
					ctx.reply('Ok. Remember it.');
				} else {
					ctx.reply('I wait a number!');
				}
			} else if (calculationStep === 2) {
					if (isDigit(ctx.message.text)) {
						b = Number(ctx.message.text);
						calculationStep = 1;
						ctx.reply(`Difference of ${a} and ${b}: ${a - b}`);
					} else {
						ctx.reply('I wait a number!');
					}
			}
			break;
		case 'multi':
			if (calculationStep === 1) {
				if (isDigit(ctx.message.text)) {
					a = Number(ctx.message.text);
					calculationStep++;
					ctx.reply('Ok. Remember it.');
				} else {
					ctx.reply('I wait a number!');
				}
			} else if (calculationStep === 2) {
					if (isDigit(ctx.message.text)) {
						b = Number(ctx.message.text);
						calculationStep = 1;
						ctx.reply(`Multi of ${a} and ${b}: ${a * b}`);
					} else {
						ctx.reply('I wait a number!');
					}
			}
			break;
		case 'div':
			if (calculationStep === 1) {
				if (isDigit(ctx.message.text)) {
					a = Number(ctx.message.text);
					calculationStep++;
					ctx.reply('Ok. Remember it.');
				} else {
					ctx.reply('I wait a number!');
				}
			} else if (calculationStep === 2) {
					if (isDigit(ctx.message.text)) {
						b = Number(ctx.message.text);
						calculationStep = 1;
						ctx.reply(`Division of ${a} and ${b}: ${a / b}`);
					} else {
						ctx.reply('I wait a number!');
					}
			}
			break;
	}
});

bot.action('sum', (ctx) => {
	state = 'sum';
	ctx.reply('Enter 2 numbers');
});

bot.action('diff', (ctx) => {
	state = 'diff';
	ctx.reply('Enter 2 numbers');
});

bot.action('multi', (ctx) => {
	state = 'multi';
	ctx.reply('Enter 2 numbers');
});

bot.action('div', (ctx) => {
	state = 'div';
	ctx.reply('Enter 2 numbers');
});

bot.startPolling();